use std::collections::HashMap;
use io_bluetooth::bt::BtListener;
use anyhow::Result;
use switch_protocol::{Server, Client, Event, Id, IdGenerator, ClientConnect, ClientDisconnect};
use switch_protocol::SendError::NoClientWithId;
use event_manager::Manager;
use protocol_packet_parser::Packet;
use crate::bluetooth_client::BluetoothClient;

pub struct BluetoothServer {
    listener: BtListener,
    events: Manager<Event>,
    id_generator: IdGenerator,
    clients: HashMap<Id, BluetoothClient>,
}

impl BluetoothServer {
    pub fn new(listener: BtListener) -> BluetoothServer {
        listener.set_nonblocking(true).unwrap();
        BluetoothServer { listener, events: Manager::new(), id_generator: IdGenerator::new(), clients: HashMap::new() }
    }
}

impl Server for BluetoothServer {
    fn update(&mut self) -> Result<()> {
        let res = self.listener.accept();
        match res {
            Err(e) => {
                if e.raw_os_error().unwrap() != 11 && e.raw_os_error().unwrap() != 10035 {
                    return Err(anyhow::Error::from(e));
                }
            }
            Ok((sock, _)) => {
                let id = self.id_generator.new_id();
                let client = BluetoothClient::new(sock);
                self.events.push(Event::ClientConnect(ClientConnect::new(id)));
                self.clients.insert(id, client);
            }
        }
        let mut to_be_removed = Vec::new();
        for (id, client) in self.clients.iter_mut() {
            let res = client.update();
            match res {
                Err(_e) => {
                    to_be_removed.push(*id);
                    continue;
                }
                Ok(_) => {}
            }
            while let Some(event) = client.poll() {
                match event {
                    Event::ReceivedPacket(mut packet) => {
                        packet.set_id(*id);
                        self.events.push(Event::ReceivedPacket(packet));
                    }
                    _ => {}
                }
            }
        }
        for id in to_be_removed.iter() {
            self.clients.remove(id).unwrap();
            self.events.push(Event::ClientDisconnect(ClientDisconnect::new(*id)));
        }
        Ok(())
    }

    fn poll(&mut self) -> Option<Event> {
        self.events.pop()
    }

    fn close_client(&mut self, id: Id) {
        self.clients.remove(&id);
    }

    fn send(&mut self, id: Id, packet: Packet) -> Result<()> {
        if let Some(client) = self.clients.get_mut(&id) {
            return client.send(packet);
        }
        Err(anyhow::Error::from(NoClientWithId { id }))
    }
}