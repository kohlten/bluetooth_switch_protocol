use io_bluetooth::bt::BtStream;
use switch_protocol::Socket;

pub struct BluetoothSocket {
    conn: BtStream
}

impl BluetoothSocket {
    pub fn new(conn: BtStream) -> Self {
        Self { conn }
    }
}

impl Socket for BluetoothSocket {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        self.conn.recv(buf)
    }

    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        self.conn.send(buf)
    }

    fn set_nonblocking(&self, nonblocking: bool) -> std::io::Result<()> {
        self.conn.set_nonblocking(nonblocking)
    }
}