use io_bluetooth::bt::BtStream;
use anyhow::Result;
use switch_protocol::{Protocol, Event, ReceivedPacket, Client};
use event_manager::Manager;
use protocol_packet_parser::Packet;
use crate::bluetooth_socket::BluetoothSocket;

pub struct BluetoothClient {
    conn: Protocol,
    events: Manager<Event>,
}

impl BluetoothClient {
    pub fn new(conn: BtStream) -> BluetoothClient {
        BluetoothClient { conn: Protocol::new(Box::new(BluetoothSocket::new(conn))), events: Manager::new() }
    }
}

impl Client for BluetoothClient {
     fn update(&mut self) -> Result<()> {
        let res = self.conn.update();
        match res {
            Err(e) => {
                return Err(e);
            }
            Ok(_) => {}
        }
        while let Some(packet) = self.conn.poll() {
            self.events.push(Event::ReceivedPacket(ReceivedPacket::new(0, packet)));
        }
        Ok(())
    }

     fn send(&mut self, packet: Packet) -> Result<()> {
        self.conn.send(packet)
    }

     fn poll(&mut self) -> Option<Event> {
        self.events.pop()
    }
}