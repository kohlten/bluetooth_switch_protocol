mod bluetooth_client;
mod bluetooth_server;
mod bluetooth_socket;
mod bluetooth_address;

pub use bluetooth_address::{BluetoothAddress, AddrParseError};
pub use bluetooth_client::BluetoothClient;
pub use bluetooth_server::BluetoothServer;
pub use bluetooth_socket::BluetoothSocket;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
