use std::str::FromStr;
use std::fmt::{Display, Formatter};
use thiserror::Error;
use io_bluetooth::bt::BtAddr;

#[derive(Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct BluetoothAddress {
    inner: [u8; 6],
}

impl BluetoothAddress {
    pub fn new(inner: [u8; 6]) -> Self {
        Self { inner }
    }

    pub fn get_inner(&self) -> [u8; 6] {
        self.inner
    }

    pub fn to_bt_addr(&self) -> BtAddr {
        BtAddr(self.inner)
    }
}

#[derive(Debug, Error)]
pub enum AddrParseError {
    #[error("failed to parse")]
    ParseError,
}

impl FromStr for BluetoothAddress {
    type Err = AddrParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.len() < 17 {
            return Err(AddrParseError::ParseError);
        }
        let mut address = [0; 6];
        let mut nums = 0;
        let mut num: [u8; 2] = [0; 2];
        let mut pos = 0;
        for char in s.chars() {
            if char.is_numeric() {
                if nums == 2 {
                    return Err(AddrParseError::ParseError);
                }
                num[nums] = char as u8;
                nums += 1;
            } else {
                if char != ':' {
                    return Err(AddrParseError::ParseError);
                }
                let res = u8::from_str(&String::from_utf8(num.to_vec()).unwrap());
                match res {
                    Ok(digit) => { address[pos] = digit; }
                    Err(_) => { return Err(AddrParseError::ParseError); }
                }
                pos += 1;
                nums = 0;
            }
        }
        let res = u8::from_str(&String::from_utf8(num.to_vec()).unwrap());
        match res {
            Ok(digit) => { address[pos] = digit; }
            Err(_) => { return Err(AddrParseError::ParseError); }
        }
        Ok(BluetoothAddress::new(address))
    }
}

impl Display for BluetoothAddress {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut output = String::new();
        for i in 0..self.inner.len() {
            output += &self.inner[i].to_string();
            if i != 5 {
                output += ":";
            }
        }
        f.write_str(&output)
    }
}

#[cfg(test)]
mod test {
    use crate::BluetoothAddress;
    use std::str::FromStr;

    #[test]
    fn test_bt_addr_from_str() {
        let res = BluetoothAddress::from_str("11:11:11:11:11:11");
        assert!(res.is_ok());
        let addr = res.unwrap();
        assert_eq!(addr.inner, [11, 11, 11, 11, 11, 11]);
        assert!(BluetoothAddress::from_str("111:11:11:11:11:1").is_err());
        assert!(BluetoothAddress::from_str("11:11:11:11:11:111").is_err());
    }

    #[test]
    fn test_bt_addr_to_str() {
        let addr = BluetoothAddress::new([11, 11, 11, 11, 11, 11]);
        assert_eq!(&addr.to_string(), "11:11:11:11:11:11");
    }
}
